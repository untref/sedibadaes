# temas de SeDiBaDaEs 2018

## mecánica

La siguiente es una lista de temas relacionados al diseño de bases de datos durante el desarrollo del seminario introduciremos los más importantes y elegiremos algunos para profundizar.

Además de los temas teóricos elegiremos ejemplos prácticos entre los que propongan los alumnos para hacer diseños sobre casos concretos. 

### • Componentes básicos de una BD

Entidades, atributos, relaciones, valores. Tablas, columnas, tipos, dominios, restricciones. Modelo de entidad relación. 

### • Tipos de datos

#### • Números

Enteros con y sin signo ¿por qué tienen un tamaño máximo? Punto flotante, punto fijo, números virtualmente infinitos. Precisión, exactitud de las operaciones. Resultados con error. Números especiales: fracciones, números complejos, coordenadas GIS. 

#### • Texto

Texto vs caracteres. Los idiomas, los alfabetos y los signos diacríticos. Texto para almacenar otros tipos de datos (ej: números y fechas). Textos largos. Textos con formato

#### • Valores lógicos

Lógica de dos estados, lógica de tres estados. Lógica booleana, operadores.

#### • Los valores nulos, omitidos, desconocidos o "no corresponde"

El valor especial `NULL`. Las diferencias semánticas de desconocido, vacío, omitido y "no corresponde". Operaciones aritméticas y lógicas con nulos. 

#### • El tiempo

Almacenamiento y uso de distintas unidades de tiempo y distintas precisiones: años, meses, fechas, momentos exactos (fecha y hora). El problema de los usos horarios y los ajustes estacionales. El problema de los calendarios (gregoriano, juliano y el momento en que fue adoptado el cambio en cada país). Pasado histórico y prehistórico. El segundo intercalar. 

### • Claves primarias

La elección de la clave primaria como problema fundamental del diseño de base de datos. El problema de los identificadores autonuméricos. Las claves compuestas. Las claves como base de la relación entre entidades. 

### • El lenguaje SQL

Definición. Lingua franca. Estandarización. Tipo de base de datos subyacente. 

Estructura, posibilidades, ejemplos

### • Transacciones y concurrencia

Definición de ACID. Bifurcación del tiempo. Consistencia

### • Normalización de bases de datos

Definición teórica de formas normales. Los efectos prácticos de la normalización. Las escasas ventajas de la redundancia de información. 

### • Metadatos

Datos sobre los datos. Metadatos de la base de datos. Metadatos de la aplicación. "El modelo de negocios". Centralizar el conocimiento sobre el sistema y las reglas del negocio

### • Bases de datos NoSQL

Las bases de datos elementales clave valor, redis. La no-estructura. Bases de datos documentales, MongoDB, CouchDb. JSON. Bases de datos híbridas, ToroDb.

La relación con BigData, los temas de velocidad y el almacenamiento distribuido, Hadoop. 

### • Bases de datos SQL modernas

Capacidades modernas: manejo tipos de datos documentales o no estructurados, almacenamiento distribuido, control automático de redundancia, alta disponibilidad, replicación, integración con GIS. 

### • Temas de permisos

Roles. Permisos por tabla, por fila, por columna o programables. Delegación de permisos. 

### • Patrones de diseño en Base de Datos

Estructuras comunes: maestro/detalle, componentes, asistentes, series de tiempo, árboles de clasificación, encuestas, clasificación geográfica, grafos, redes. 

### • Temas de seguridad

Vulnerabilidades del uso de base de datos. Actividades de los hackers. La relación con los lenguajes de programación. 

### • Diseño ágil

Diseñar para el cambio. Metodologías ágiles.

### • Consolidación de bases de datos

Empezar a trabajar sin tener todo listo (o sin tener el diseño listo). Bases de datos de distintos orígenes. Interconexión entre sistemas. 

<img style="float: right;" align="right" src="imagenes/logo-untref-w.png">

# untref/sedibadaes

* **Universidad Nacional de Tres de Febrero**
* **Maestría en Generación y Análisis de Información Estadística**

# Seminario de Diseño de Bases de Datos Estadísticas

## Clases

   * *Las clases se dictarán los últimos martes de cada mes hasta noviembre de 2018 en el aula 4 de Viamonte*

   * Se alternarán temas teóriocos y prácticos de diseño de bases de datos [*ver acá la lista de posibles [temas](temas.md)*]. Se priorizarán ejemplos prácticos de temas propuestos por los alumnos. 

   
  fecha| pdf  | contenido de la clase
-------|------|----------
24/4   |      | Introducción al diseño de bases de datos
29/5   |      |
26/6   |      |
31/7   |      |
28/8   |      |
25/9   |      |
30/10  |      |
27/11  |      |


## Recursos

* [Software necesario para la materia](software.md#software-a-instalar-untref-siybd-2017)
* [Sitios](sitios.md#sitios-de-referencia-untref-siybd-2016) de referencia, con [información](sitios.md#información) o [herramientas](sitios.md#herramientas)
* [Bibliografía](bibliografia.md)


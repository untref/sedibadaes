# sitios de referencia (SeDiBaDaEs 2018)

## información

* [Dirección General de Estadísticas y Censos](https://www.estadisticaciudad.gob.ar/eyc/) de la Ciudad de Buenos Aires
* [Instituto Nacional de Estadísticas y Censos](https://www.indec.gob.ar/)
* [CEPALSTAT](http://estadisticas.cepal.org/cepalstat/Portada.html) 

## herramientas

Estos sitios contienen herramientas en línea, 
es recomendable registrarse para poder guardar el trabajo realizado o las opciones de configuración

   * [![gitlab](https://about.gitlab.com/ico/favicon-32x32.png) https://gitlab.com/](https://gitlab.com/)
   * [![rextexter](imagenes/rextester.png) http://rextester.com/l/postgresql_online_compiler](http://rextester.com/l/postgresql_online_compiler)
   * [![jsfiddle](https://jsfiddle.net/img/favicon.png) https://jsfiddle.net/](https://jsfiddle.net/)
   * [![txt-to-sql](https://codenautas.com/codenautas_favicon-01.png) https://codenautas.com/txt-to-sql](https://codenautas.com/txt-to-sql)
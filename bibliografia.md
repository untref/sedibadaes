# Bibliografía de SeDiBaDaEs 2018

Celko, J (1999). *Data & Databases: Concepts in practice*. San Francisco, California. Morgan Kaufmann Publishers.

Teorey T.J., Buxton S., Fryman L... (2009). *Database Design. know it all*. Morgan Kaufmann Publishers.

Celko, J (2010). *SQL for Smarties, Advanced SQL Programming, 4th Edition*. San Francisco, California. Morgan Kaufmann Publishers.

García-Molina, H., Ullman Jeffrey D., Widom J.  (2008) *Database Systems The complete book, 2nd Edition*. Stanford University. Prentice Hall.